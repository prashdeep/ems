        package com.edureka.ems.dao;

        import com.edureka.ems.model.Dependent;
        import com.edureka.ems.model.Employee;
        import com.edureka.ems.model.EmploymentStatus;
        import com.edureka.ems.model.Project;
        import com.edureka.ems.util.EmployeeMapper;
        import com.edureka.ems.util.JDBCUtil;
        import com.edureka.ems.util.ProjectMapper;

        import java.sql.*;
        import java.text.SimpleDateFormat;
        import java.util.ArrayList;
        import java.util.List;
        import java.util.SimpleTimeZone;

        /**
         * Created by prade on 4/27/2018.
         */
        public class EmployeeDAOImpl implements EmployeeDAO {

            private static final String FETCH_EMPLOYEES_BY_MGR_ID = "select * from Employee where mgr_id=?";

            private static final String FETCH_EMPLOYEE_BY_ID = "select * from Employee where id=?";

            private static final String UPDATE_DEPENDENT_BY_EMP_ID = "insert into dependents(name, relationship, dob, contact, emp_id) values(?,?,?,?,?)";

            private static final String INSERT_COMMUTE_DETAILS_BY_EMP_ID = "insert into commute_details(emp_Id, pickup_point) values (?,?)";

            private static final String UPDATE_COMMUTE_DETAILS_BY_EMP_ID = "update commute_details set pickup_point = ? where emp_id = ?";

            private static final String FETCH_COMMUTE_DETAILS_BY_EMP_ID = "select * from commute_details where emp_id=?";

            private static final String UPDATE_PROJECT_FOR_EMP_ID="update employee set project_id=?, project_mapped_date=? where id=?";

            private static final String FETCH_ALL_PROJECTS = "select * from project";

            private static final String APPLY_FOR_RESIGNATION="update employee set employement_status='RESIGNATION_PENDING' where id=?";

            private static final String VIEW_RESIGNATION_STATUS="select employement_status from employee where id=?";

            private static final String FETCH_RESIGNED_EMPLOYEES="select * from employee where mgr_id=? and employement_status='RESIGNATION_PENDING'";

            private static final String UPDATE_EMPLOYEMENT_STATUS="update employee set employement_status=? where id=?";



            public List<Employee> listAll() {
                return JDBCUtil.fetchEmployees();
            }

            public Employee fetch(int id) {
                try {
                    PreparedStatement preparedStatement = JDBCUtil.getConnection().prepareStatement(FETCH_EMPLOYEE_BY_ID);
                    preparedStatement.setInt(1, id);
                   ResultSet resultSet =  preparedStatement.executeQuery();
                    while (resultSet.next()){
                        Employee employee = EmployeeMapper.mapRow(resultSet);
                        return  employee;
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return null;
            }


            public void updateDependents(int empId, Dependent dependent) {
              try {
                    PreparedStatement preparedStatement = JDBCUtil.getConnection().prepareStatement(UPDATE_DEPENDENT_BY_EMP_ID);
                    preparedStatement.setString(1, dependent.getName());
                    preparedStatement.setString(2, dependent.getRelationship().toString());
                    if(dependent.getDob() != null) {
                        preparedStatement.setDate(3, new java.sql.Date(dependent.getDob().getTime()));
                    }else {
                        preparedStatement.setDate(3, null);
                    }
                    preparedStatement.setString(4, dependent.getContact());
                    preparedStatement.setInt(5, dependent.getEmpId());

                    preparedStatement.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            public List<Employee> listAllEmployeesReporting(int mgrId) {
                List<Employee> employeeList = new ArrayList<>();
                try {
                    PreparedStatement preparedStatement = JDBCUtil.getConnection().prepareStatement(FETCH_EMPLOYEES_BY_MGR_ID);
                    preparedStatement.setInt(1, mgrId);
                    ResultSet resultSet =  preparedStatement.executeQuery();

                    while (resultSet.next()){
                        Employee employee = EmployeeMapper.mapRow(resultSet);
                        employeeList.add(employee);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return employeeList;
            }

            @Override
            public void availCommute(int empId, String pickupPoint) {
                System.out.println("Came iside");
                try {
                    //first fetch if the employee has already availed the commute facility
                    PreparedStatement preparedStatement = JDBCUtil.getConnection().prepareStatement(FETCH_COMMUTE_DETAILS_BY_EMP_ID);
                    preparedStatement.setInt(1, empId);
                    ResultSet resultSet = preparedStatement.executeQuery();
                    if (resultSet != null && resultSet.next()){
                        preparedStatement = JDBCUtil.getConnection().prepareStatement(UPDATE_COMMUTE_DETAILS_BY_EMP_ID);
                        preparedStatement.setString(1, pickupPoint);
                        preparedStatement.setInt(2, empId);
                        preparedStatement.executeUpdate();
                    } else {
                        preparedStatement = JDBCUtil.getConnection().prepareStatement(INSERT_COMMUTE_DETAILS_BY_EMP_ID);
                        preparedStatement.setInt(1, empId);
                        preparedStatement.setString(2, pickupPoint);
                        preparedStatement.executeUpdate();
                    }

                } catch (SQLException e){
                    System.out.println("exception while updating commute details "+e);
                }
            }

           public  List<Project> listAllProjects(){
               List<Project> projectList = new ArrayList<>();
                try{
                    Statement statement = JDBCUtil.getConnection().createStatement();
                    ResultSet resultSet = statement.executeQuery(FETCH_ALL_PROJECTS);
                    while (resultSet.next()){
                        Project project = ProjectMapper.mapRow(resultSet);
                        projectList.add(project);
                    }
                }catch (SQLException exception){
                    System.out.println("Exception while fetching the project details");
                }
                return projectList;
            }

            public boolean switchProject(int empId, int projectId){
               try {
                   PreparedStatement preparedStatement = JDBCUtil.getConnection().prepareStatement(UPDATE_PROJECT_FOR_EMP_ID);
                   preparedStatement.setInt(1, projectId);
                   preparedStatement.setDate(2, new Date(new java.util.Date().getTime()));
                   preparedStatement.setInt(3, empId);
                   int updatedRows = preparedStatement.executeUpdate();
                   if (updatedRows > 0 ){
                       return true;
                   } else {
                       return false;
                   }
               }catch (SQLException exception){
                   System.out.println("Exception while updating the project details");
               }
               return false;
            }

            public void applyResignation(int empId){
                try{
                    PreparedStatement preparedStatement = JDBCUtil.getConnection().prepareStatement(APPLY_FOR_RESIGNATION);
                    preparedStatement.setInt(1, empId);
                    preparedStatement.execute();
                } catch (SQLException exception){
                    System.out.println("Exception while setting the status to Resignation pending for employee "+empId);
                }
            }

            public EmploymentStatus viewResignationStatus(int empId){
                try{
                    PreparedStatement preparedStatement = JDBCUtil.getConnection().prepareStatement(VIEW_RESIGNATION_STATUS);
                    preparedStatement.setInt(1, empId);
                    ResultSet resultSet = preparedStatement.executeQuery();
                    resultSet.next();
                    return EmploymentStatus.valueOf(resultSet.getString(1));
                 } catch (SQLException exception){
                    System.out.println("Exception while setting the status to Resignation pending for employee "+empId);
                    exception.printStackTrace();
                }
                return  null;
            }

            public List<Employee> fetchResignedEmployees(int empId){
                List<Employee> employeeList = new ArrayList<>();
                try{
                    PreparedStatement preparedStatement = JDBCUtil.getConnection().prepareStatement(FETCH_RESIGNED_EMPLOYEES);
                    preparedStatement.setInt(1, empId);
                    ResultSet resultSet = preparedStatement.executeQuery();
                    while (resultSet.next()){
                        Employee employee = EmployeeMapper.mapRow(resultSet);
                        employeeList.add(employee);
                    }

                } catch (SQLException exception){
                    System.out.println("Exception while fetching Resignation employees for manager "+empId);
                    exception.printStackTrace();
                }
                return  employeeList;

            }

            public  void updateEmployementStatus(Employee employee){
                try{
                    PreparedStatement preparedStatement = JDBCUtil.getConnection().prepareStatement(UPDATE_EMPLOYEMENT_STATUS);
                    preparedStatement.setString(1, employee.getEmploymentStatus().toString());
                    preparedStatement.setInt(2, employee.getId());
                    preparedStatement.executeUpdate();
                } catch (SQLException exception){
                    System.out.println("Exception while updating the resignation status for "+employee.getId());
                    exception.printStackTrace();
                }
            }
        }
