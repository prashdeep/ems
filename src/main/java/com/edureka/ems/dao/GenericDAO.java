package com.edureka.ems.dao;

import java.util.List;

/**
 * Created by prade on 4/27/2018.
 */
public interface GenericDAO<T> {
    public List<T> listAll();
    public T fetch(int id);

}
