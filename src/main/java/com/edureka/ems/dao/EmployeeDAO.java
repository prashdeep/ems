package com.edureka.ems.dao;

import com.edureka.ems.model.Dependent;
import com.edureka.ems.model.Employee;
import com.edureka.ems.model.EmploymentStatus;
import com.edureka.ems.model.Project;

import java.util.List;

/**
 * Created by prade on 4/27/2018.
 */
public interface EmployeeDAO extends GenericDAO<Employee> {
    void updateDependents(int empId, Dependent dependent);
    List<Employee> listAllEmployeesReporting(int mgrId);

    void availCommute(int empId, String pickupPoint);

    List<Project> listAllProjects();

    boolean switchProject(int empId, int projectId);

    void applyResignation(int empId);

    EmploymentStatus viewResignationStatus(int empId);

    List<Employee> fetchResignedEmployees(int empId);

    void updateEmployementStatus(Employee employee);
}
