package com.edureka.ems.model;

import java.util.Date;

/**
 * Created by prade on 5/16/2018.
 */
public class Project {

    private int projectId;
    private String projectName;
    private String client;
    private Date projectStartDate;

    public Project() {
    }

    public Project(int projectId, String projectName, String client, Date projectStartDate) {
        this.projectId = projectId;
        this.projectName = projectName;
        this.client = client;
        this.projectStartDate = projectStartDate;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public Date getProjectStartDate() {
        return projectStartDate;
    }

    public void setProjectStartDate(Date projectStartDate) {
        this.projectStartDate = projectStartDate;
    }


}
