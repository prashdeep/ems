package com.edureka.ems.model;

import com.edureka.ems.service.EmployeeService;

import java.util.Date;

/**
 * Created by prade on 4/26/2018.
 */
public class Employee {
    private int id;
    private String name;
    private int deptId;
    private Date dateOfJoining;
    private Date dateOfRelieving;
    private String email;
    private String primaryContact;
    private String emergencyContact;
    private int mgrId;
    private int projectId;
    private Date projectMappedDate;
    private EmploymentStatus employmentStatus;
    private String role;

    public Employee(int id, String name, int deptId, Date dateOfJoining, Date dateOfRelieving, String email, String primaryContact, String emergencyContact, int mgrId, int projectId, Date projectMappedDate, EmploymentStatus employmentStatus, String role) {
        this.id = id;
        this.name = name;
        this.deptId = deptId;
        this.dateOfJoining = dateOfJoining;
        this.dateOfRelieving = dateOfRelieving;
        this.email = email;
        this.primaryContact = primaryContact;
        this.emergencyContact = emergencyContact;
        this.mgrId = mgrId;
        this.projectId = projectId;
        this.projectMappedDate = projectMappedDate;
        this.employmentStatus = employmentStatus;
        this.role = role;
    }

    public int getId() {
        return id;

    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    public Date getDateOfJoining() {
        return dateOfJoining;
    }

    public void setDateOfJoining(Date dateOfJoining) {
        this.dateOfJoining = dateOfJoining;
    }

    public Date getDateOfRelieving() {
        return dateOfRelieving;
    }

    public void setDateOfRelieving(Date dateOfRelieving) {
        this.dateOfRelieving = dateOfRelieving;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPrimaryContact() {
        return primaryContact;
    }

    public void setPrimaryContact(String primaryContact) {
        this.primaryContact = primaryContact;
    }

    public String getEmergencyContact() {
        return emergencyContact;
    }

    public void setEmergencyContact(String emergencyContact) {
        this.emergencyContact = emergencyContact;
    }

    public int getMgrId() {
        return mgrId;
    }

    public void setMgrId(int mgrId) {
        this.mgrId = mgrId;
    }

    public Date getProjectMappedDate() {
        return projectMappedDate;
    }

    public void setProjectMappedDate(Date projectMappedDate) {
        this.projectMappedDate = projectMappedDate;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public EmploymentStatus getEmploymentStatus() {
        return employmentStatus;
    }

    public void setEmploymentStatus(EmploymentStatus employmentStatus) {
        this.employmentStatus = employmentStatus;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", deptId=" + deptId +
                ", dateOfJoining=" + dateOfJoining +
                ", dateOfRelieving=" + dateOfRelieving +
                ", email='" + email + '\'' +
                ", primaryContact='" + primaryContact + '\'' +
                ", emergencyContact='" + emergencyContact + '\'' +
                ", mgrId=" + mgrId +
                ", projectId=" + projectId +
                ", projectMappedDate=" + projectMappedDate +
                ", employmentStatus=" + employmentStatus +
                ", role='" + role + '\'' +
                '}';
    }
}
