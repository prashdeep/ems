package com.edureka.ems.model;

/**
 * Created by prade on 4/27/2018.
 */
public enum EmploymentStatus {
    PROBATION,
    FULL_TIME,
    RESIGNATION_PENDING,
    RESIGNATION_APPROVED,
    RESIGNATION_REJECTED
}
