package com.edureka.ems.model;

/**
 * Created by prade on 4/27/2018.
 */
public enum Relationship {
    FATHER,
    MOTHER,
    SPOUSE,
    SON,
    DAUGHTER
}
