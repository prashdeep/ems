package com.edureka.ems.model;

import java.util.Date;

/**
 * Created by prade on 4/27/2018.
 */
public class Dependent {
    private int id;
    private String name;
    private Relationship relationship;
    private Date dob;
    private String contact;
    private int empId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Relationship getRelationship() {
        return relationship;
    }

    public void setRelationship(Relationship relationship) {
        this.relationship = relationship;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    @Override
    public String toString() {
        return "Dependent{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", relationship=" + relationship +
                ", dob=" + dob +
                ", contact='" + contact + '\'' +
                ", empId=" + empId +
                '}';
    }
}


