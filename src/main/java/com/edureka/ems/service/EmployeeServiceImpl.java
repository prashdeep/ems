package com.edureka.ems.service;

import com.edureka.ems.dao.EmployeeDAO;
import com.edureka.ems.model.Dependent;
import com.edureka.ems.model.Employee;
import com.edureka.ems.model.EmploymentStatus;
import com.edureka.ems.model.Project;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by prade on 4/27/2018.
 */
public class EmployeeServiceImpl implements EmployeeService {

    private EmployeeDAO employeeDAO;

    public EmployeeServiceImpl(EmployeeDAO employeeDAO){
        this.employeeDAO = employeeDAO;
    }
    public List<Employee> listAllEmployees() {
        List<Employee> employees = this.employeeDAO.listAll();
        return employees;
    }

    public Employee fetchEmployeeDetails(int empId) {
        return this.employeeDAO.fetch(empId);
    }

    public void updateDependents(int empId, Dependent dependent) {
        this.employeeDAO.updateDependents(empId, dependent);
    }

    @Override
    public void availCommute(int empId, String pickupPoint) {
        this.employeeDAO.availCommute(empId, pickupPoint);
    }

    public List<Employee> listEmployeesReporting(int mgrId) {
        return this.employeeDAO.listAllEmployeesReporting(mgrId);

    }

    public boolean switchProject(int empId, int projectId) {
        Employee employee = employeeDAO.fetch(empId);
        Date projectMappedDate = employee.getProjectMappedDate();
        Calendar calendar = Calendar.getInstance();
        calendar.add( Calendar.MONTH ,  -6 );
        if (projectMappedDate.compareTo( calendar.getTime() ) < 0){
            return this.employeeDAO.switchProject(empId, projectId);
        } else{
            System.out.println("You still have not completed 6 months to apply for changing the project");
            return false;
        }
    }

    public void applyResignation(int empId) {
        this.employeeDAO.applyResignation(empId);

    }

    public EmploymentStatus viewResignationStatus(int empId) {
        return this.employeeDAO.viewResignationStatus(empId);
    }

    public List<Project> listAllProjects(){
        return this.employeeDAO.listAllProjects();
    }

    public List<Employee> fetchEmployeesForResignationApproval(int empId){
      return this.employeeDAO.fetchResignedEmployees(empId);
    }

    public void updateEmployementStatus(Employee employee){
        this.employeeDAO.updateEmployementStatus(employee);
    }
}
