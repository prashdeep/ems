package com.edureka.ems.service;

import com.edureka.ems.model.Dependent;
import com.edureka.ems.model.Employee;
import com.edureka.ems.model.EmploymentStatus;
import com.edureka.ems.model.Project;

import java.util.List;

/**
 * Created by prade on 4/27/2018.
 */
public interface EmployeeService {

    public List<Employee> listAllEmployees();

    public Employee fetchEmployeeDetails(int empId);

    public void updateDependents(int empId, Dependent dependent);

    public void availCommute(int empId, String pickupPoint);

    public List<Employee> listEmployeesReporting(int mgrId);

    public boolean switchProject(int empId, int projectId);

    public void applyResignation(int empId);

    public EmploymentStatus viewResignationStatus(int empId);

    List<Project> listAllProjects();

    List<Employee> fetchEmployeesForResignationApproval(int empId);

    void updateEmployementStatus(Employee employee);
}
