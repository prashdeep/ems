package com.edureka.ems.util;
import com.edureka.ems.dao.EmployeeDAOImpl;
import com.edureka.ems.model.*;
import com.edureka.ems.service.EmployeeService;
import com.edureka.ems.service.EmployeeServiceImpl;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;


/**
 * Created by prade on 4/27/2018.
 */
public class CliClient {
    /**
     * The main entry point.
     * @param ar the list of arguments
     *
     */
    private static Scanner option = new Scanner(System.in, "UTF-8");

    private static final EmployeeService service = new EmployeeServiceImpl(new EmployeeDAOImpl());
    public static void main(final String[] ar) {

        System.out.println("Welcome to Employee Manangement System");
        System.out.println("-----------------------");
        System.out.println("1. List All Employees Info");
        System.out.println("2. Display Employee Info");
        System.out.println("3. Update Dependent Details");
        System.out.println("4. Avail Commute facility");
        System.out.println("5. View All Employees reporting to you");
        System.out.println("6. Switch Project");
        System.out.println("7. Apply For Resignation");
        System.out.println("8. View Resignation status");
        System.out.println("9. Approve/Reject Resignation");
        System.out.println("10. Exit");
        System.out.println("Enter your choice:");
        int menuOption = option.nextInt();
        mainMenuDetails(menuOption);
    }

    private static void mainMenuDetails(final int selectedOption) {
        switch (selectedOption) {
            case 1:
                listAllEmployees();
                break;
            case 2:
                fetchEmployeeDetails();
                break;
            case 3:
                updateDependentDetails();
                break;
            case 4:
                availCommute();
                break;
            case 5:
                viewReportingEmployees();
                break;
            case 6:
                switchProject();
                break;
            case 7:
                applyResignation();
            case 8:
                viewResignationStatus();
                break;
            case 9:
                approveRejectResignation();
                break;
            case 10:
                Runtime.getRuntime().halt(0);
            default:
                System.out.println("Choose valid options");
        }
    }



    private static void availCommute() {
        System.out.println("Enter your Employee ID ");
        int empId = option.nextInt();
        if(isRegularEmployee(empId)) {
            System.out.println("Please enter your pickup/drop point");
            String pickupDetails = option.next();
            service.availCommute(empId, pickupDetails);
        }else {
            System.out.println("Cannot avail commute for Resigned employee");
        }
    }

    private static void viewResignationStatus() {
        System.out.println("Enter your Employee ID ");
        int empId = option.nextInt();
        EmploymentStatus employmentStatus = service.viewResignationStatus(empId);
        System.out.println("Employement status is :: "+employmentStatus.toString());
    }

    private static void applyResignation() {
        System.out.println("Please enter your employee Id ");
        int empId = option.nextInt();
        service.applyResignation(empId);
    }

    private static void switchProject() {
        System.out.println("Please enter your employeeId");
        int empId = option.nextInt();
        if(isRegularEmployee(empId)) {
            Iterator<Project> iterator = service.listAllProjects().iterator();
            while (iterator.hasNext()) {
                Project project = iterator.next();
                System.out.println("------------------------------------------");
                System.out.println("Project ID: " + project.getProjectId());
                System.out.println("Project Name : " + project.getProjectName());
                System.out.println("Client Name: " + project.getClient());
                System.out.println("Project Start Date: " + new SimpleDateFormat("dd-MM-yyyy").format(project.getProjectStartDate()));
                System.out.println("------------------------------------------");
            }
            System.out.println("Please select a project Id to move to ::");
            int projectId = option.nextInt();
            service.switchProject(empId, projectId);
        } else{
            System.out.println("Cannot switch projects for Resigned employee");
        }

    }

    private static void viewReportingEmployees() {
        System.out.println("Enter your Employee ID ");
        int empId = option.nextInt();
        List<Employee> employeeList = service.listEmployeesReporting(empId);
        System.out.println(employeeList);
    }

    private static void updateDependentDetails(){
        System.out.println("Please enter the Employee ID");
        int empId = option.nextInt();
        if(isRegularEmployee(empId)) {
            Dependent dependent = new Dependent();
            System.out.println("Please enter the dependents's name");
            String name = option.next();
            dependent.setName(name);
            System.out.println("Please enter the dependent's contact");
            String contact = option.next();
            dependent.setContact(contact);

            dependent.setEmpId(empId);
            System.out.println("Please enter the date of Birth in dd/MM/YY format");
            String strDOB = option.next();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
            Date dob = null;
            try {
                dob = dateFormat.parse(strDOB);
            } catch (Exception ex) {
                System.out.println("Invalid DOB format");
            }
            dependent.setDob(dob);
            System.out.println("Please enter the relationship with the Employee");
            String strRelationship = option.next();
            Relationship relationship = Relationship.valueOf(strRelationship);
            dependent.setRelationship(relationship);
            service.updateDependents(empId, dependent);
        } else {
            System.out.println("Cannot update Dependents for Resigned employee");
        }
    }

    private static void fetchEmployeeDetails() {
        System.out.println("Enter the empId for fetching the details");
        int empId = option.nextInt();
        Employee employee = service.fetchEmployeeDetails(empId);
        System.out.println(employee);
    }

    private static void listAllEmployees() {
       List<Employee> listOfEmployees = service.listAllEmployees();
        System.out.println(listOfEmployees);
    }

    private static void approveRejectResignation() {
        System.out.println("Please enter your employeeID");
        int empId = option.nextInt();
        Employee manager = service.fetchEmployeeDetails(empId);
        if(manager.getRole().equalsIgnoreCase("Manager") || manager.getRole().equalsIgnoreCase("Admin")){
            List<Employee> employeeList = service.fetchEmployeesForResignationApproval(empId);
            Iterator<Employee> iterator = employeeList.iterator();
            while (iterator.hasNext()){
                Employee employee = iterator.next();
                System.out.println("********************************");
                System.out.println("Employee Name : "+employee.getName());
                System.out.println("Employee Date Of Joining : "+employee.getDateOfJoining());
                System.out.println("Employee Project Details : "+employee.getProjectId());
                System.out.println("Employee Employement Status : "+employee.getEmploymentStatus());
                System.out.println("********************************");
                System.out.println("Please enter 1 for approve and 2 for reject");
                int input = option.nextInt();
                if(input == 1){
                    employee.setEmploymentStatus(EmploymentStatus.RESIGNATION_APPROVED);
                } else if (input == 2){
                    employee.setEmploymentStatus(EmploymentStatus.RESIGNATION_REJECTED);
                }
                service.updateEmployementStatus(employee);
            }
        } else{
            System.out.println("You are not authorized to approve or reject the resignations");
        }
    }
    private static boolean isRegularEmployee(int empId){
        Employee employee = service.fetchEmployeeDetails(empId);
        return employee.getEmploymentStatus() == EmploymentStatus.RESIGNATION_APPROVED ? false: true;

    }
}
