package com.edureka.ems.util;

import com.edureka.ems.model.Project;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by prade on 5/16/2018.
 */
public class ProjectMapper {

    public static Project mapRow(ResultSet resultSet){
        try {
            int projectId = resultSet.getInt(1);
            String projectName = resultSet.getString(2);
            Date projectStartDate = resultSet.getDate(3);
            String client = resultSet.getString(4);
            return new Project(projectId, projectName, client, new Date(projectStartDate.getTime()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new Project();
    }
}
