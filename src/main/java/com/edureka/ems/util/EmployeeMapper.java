package com.edureka.ems.util;

import com.edureka.ems.model.Employee;
import com.edureka.ems.model.EmploymentStatus;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by prade on 4/27/2018.
 */
public class EmployeeMapper {

    public static Employee mapRow(ResultSet resultSet){
        try {
            int empId = resultSet.getInt(1);
            String empName = resultSet.getString(2);
            int depId = resultSet.getInt(3);
            Date empJoiningDate = resultSet.getDate(4);
            Date empRelivingDate = resultSet.getDate(5);
            String email = resultSet.getString(6);
            String primaryContact = resultSet.getString(7);
            String emergencyContact = resultSet.getString(8);
            int managerId = resultSet.getInt(9);
            int projectId = resultSet.getInt(10);
            Date projectMappedDate = resultSet.getDate(11);
            String role = resultSet.getString(12);
            EmploymentStatus employmentStatus = EmploymentStatus.valueOf(resultSet.getString(13));

            Employee employee = new Employee(empId,empName,depId, empJoiningDate,empRelivingDate, email, primaryContact, emergencyContact,managerId, projectId, projectMappedDate, employmentStatus,role);
            return employee;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
