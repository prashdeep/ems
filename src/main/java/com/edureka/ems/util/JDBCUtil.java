package com.edureka.ems.util;

import com.edureka.ems.model.Employee;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by prade on 4/27/2018.
 */
public class JDBCUtil {

    public static final String DATABASE_NAME = "ems";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "welcome";

    private static final String FETCH_ALL_EMPLOYEES = "select * from Employee";




    public static Connection getConnection(){
        Connection connection = null;
        try{
            connection = DriverManager.getConnection("jdbc:mysql://localhost/"+DATABASE_NAME, USERNAME, PASSWORD);
        } catch (SQLException e) {
            System.out.println("Exception connection to the Database :: " +e.getMessage());
            e.printStackTrace();
        }
        return connection;
    }

    public boolean execute(String sql) {
        try(Statement statement = getStatement();){
            return statement.execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static final List<Employee> fetchEmployees(){
        ResultSet resultSet = null;
        List<Employee> employeeList = new ArrayList<>();
        try (Statement statement = getStatement()) {
            resultSet = statement.executeQuery(FETCH_ALL_EMPLOYEES);
            while (resultSet.next()){
                Employee employee = EmployeeMapper.mapRow(resultSet);
                employeeList.add(employee);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return employeeList;
    }

    private static Statement getStatement(){
        Connection connection = getConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return statement;
    }
}
