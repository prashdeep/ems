create database if not exists ems;
use ems;


CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `dep_id` int(11) DEFAULT NULL,
  `doj` date NOT NULL,
  `dor` date DEFAULT NULL,
  `email` varchar(40) NOT NULL,
  `primary_contact` varchar(14) NOT NULL,
  `emergency_contact` varchar(14) NOT NULL,
  `mgr_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `project_mapped_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role` enum('NORMAL','MANAGER','ADMIN') DEFAULT 'NORMAL',
  `employement_status` enum('PROBATION','FULL_TIME','RESIGNATION_PENDING','RESIGNATION_APPROVED','RESIGNATION_REJECTED') DEFAULT 'FULL_TIME',
  PRIMARY KEY (`id`),
  KEY `dept_id_fk` (`dep_id`),
  KEY `mgr_id_fk` (`mgr_id`),
  KEY `project_id_fk` (`project_id`),
  CONSTRAINT `dept_id_fk` FOREIGN KEY (`dep_id`) REFERENCES `department` (`id`),
  CONSTRAINT `mgr_id_fk` FOREIGN KEY (`mgr_id`) REFERENCES `employee` (`id`),
  CONSTRAINT `project_id_fk` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `project` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(20) DEFAULT NULL,
  `project_start_date` date DEFAULT NULL,
  `client` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `dependents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `relationship` varchar(20) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `contact` varchar(14) NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_id_fk` (`emp_id`),
  CONSTRAINT `emp_id_fk` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


CREATE TABLE `commute_details` (
  `pickup_point` varchar(20) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL,
  KEY `emp_id_fk_cd` (`emp_id`),
  CONSTRAINT `emp_id_fk_cd` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `location` varchar(30) DEFAULT NULL,
  `director` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `head_id_fk` (`director`),
  CONSTRAINT `head_id_fk` FOREIGN KEY (`director`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into project (project_name, client, project_start_date) values
  ("testing", "Vidyo", "2018-06-10");

insert into employee(name, doj, email, primary_contact, emergency_contact, mgr_id) values
  ('Vikram', '2018-12-04', 'vikram@gmail.com', '9632565424', '251254585', 1),
  ('Naveen', '2018-12-04', 'naveen@gmail.com', '9632565424', '251254585', 1);

insert into dependents(name, relationship, contact, emp_id) values("harish", "SON","9632532356", 1);