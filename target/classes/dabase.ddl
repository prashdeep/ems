create database if not exists ems;
use ems;

create table employee(
  id int primary key auto_increment,
  name varchar(40) not null,
  dep_id int,
  doj date not null,
  dor date,
  email varchar(40) not null,
  primary_contact varchar (14) not null,
  emergency_contact varchar (14) not null,
  mgr_id int,
  constraint dept_id_fk foreign key (dep_id) references department(id),
  constraint mgr_id_fk foreign key (mgr_id) references employee(id)
);


create table department(
  id int primary key auto_increment,
  name varchar(40) not null,
  location varchar(30)
  -- director int not null,
  -- constraint head_id_fk foreign key (director) references employee (id)
);

create table dependents(
  id int primary key auto_increment,
  name varchar(40) not null,
  relationship varchar(20),
  dob date,
  contact varchar(14) not null,
  emp_id int,
  constraint emp_id_fk foreign key (emp_id) references employee(id)
);

alter table department add column director int not null;

alter table department add constraint head_id_fk foreign key(director) references employee(id);